// This is a comment. Comments begin with "//", and tell the compiler not to
// treat them as code. Use comments to clarify your code to yourself or others.


// This line is used to include a library. Many libraries already reside on your
// computer, and provide extra functions and code that you can use in your
// programs. The iostream library is one of the most-used C++ libraries, and it
// includes common functions for reading from and writing to the command line.
#include <iostream>

// This line tells the compiler to use the "std" namespace. You can think of a
// namespace as a sort of room that certain functions can live in. If we didn't
// have this line, we would have to prefix all of the functions in the iostream
// library with "std::" so the compiler knows where to look for each function.
using namespace std;

// This is the start of our main function. Every C++ program needs a function
// called "main" that is automatically called when the program is run. The body
// of this function is where we can put code that calls other functions or
// performs computations. We can also tell that the main function returns an
// integer when it is done by the presence of the "int" keyword at the beginning
// of the line.
int main() {
  // This line uses the "cout" function, found in the iostream library, to print
  // a string on the command line. Play around with changing the contents of the
  // string, and you'll see the change when you recompile and run the program.
  // Also, the "endl" after the string stands for "end line", and tells the
  // command to make a new line. Try adding another endl (" << endl") before
  // the semicolon. What do you think will happen now? Go ahead and recompile
  // and run the program, and you should see that there is now an extra line
  // printed on the screen.
  cout << "Hello world!" << endl;

  // This return value is traditionally used to tell the command line whether or
  // not the program has completed successfully. Returning 0 means success while
  // any nonzero number means there was an error.
  return 0;
}
